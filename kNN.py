from sklearn import datasets
from sklearn.preprocessing import scale
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.dummy import DummyClassifier
from sklearn.model_selection import KFold
from sklearn.model_selection import cross_validate


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-muted')#dark_background')#fivethirtyeight')#ggplot')



###
'''
1. [x] import scikit-learn data (as bunch, which is dict-like)
2. [x]scale data
3. [x] do ml algorithms (knn)
4. [x] score
5. [x] confusion matrix
6. [ ] validation stuff
7. [x] feature importance
8. [ ] plot data somehow showing correct/incorrect
9. [x] dummy classifier as baseline
'''



# From http://scikit-learn.org/stable/datasets/index.html#toy-datasets
'''
load_iris([return_X_y]) Load and return the iris dataset (classification).
load_digits([n_class, return_X_y])  Load and return the digits dataset (classification).
load_wine([return_X_y]) Load and return the wine dataset (classification).
load_breast_cancer([return_X_y])    Load and return the breast cancer wisconsin dataset (classification).
load_boston([return_X_y])   Load and return the boston house-prices dataset (regression).
load_diabetes([return_X_y]) Load and return the diabetes dataset (regression).
load_linnerud([return_X_y]) Load and return the linnerud dataset (multivariate regression).
'''
toydataset = 'iris' # iris, digits, wine, breast_cancer
# digits has no feature_names

# Load data as Bunch (dict-like) via eval() so all I need to do to look at new data is change the above str
Data = eval('datasets.load_'+toydataset+'()')
print('')
print(Data.DESCR)
print('Data.keys() = ', Data.keys())
# Separate Data into features and associated target
X = Data.data
y = Data.target


# Data.keys()
# Plot a feature*feature matrix. Diagonal are just histos.
df = pd.DataFrame(X, columns=Data.feature_names)
pd.scatter_matrix(df, c=y, figsize=(20,8), s=100, marker='.') # figsize = (inches,inches)
# Create plots with empty data, then use those points for labels...?
#handles = [plt.plot([],[],color=plt.cm.brg(i/2.), ls="", marker=".", \
#                    markersize=np.sqrt(10))[0] for i in range(3)]
handles = [plt.scatter([],[], c=plt.cm.brg(i/2.), marker='.', s=100) for i in range(3)]
labels = Data.target_names
plt.legend(handles, labels, loc=(1.02,0))
#plt.savefig(toydataset+'/kNN_'+toydataset+'_scatter.png')
#plt.show()

# Or plot via seaborn...
import seaborn as sns
sns.set(style="ticks")
df2 = sns.load_dataset("iris")
sns.pairplot(df2, hue="species")
plt.savefig(toydataset+'/kNN_'+toydataset+'_scatter.png')









# Scale data
X = scale(X)

# Split data (and associated target) into training and test data randomly
# "Stratify the split according to the labels so that they are distributed in the training and test sets as they are in the original dataset."
split = 2/3.
X_train, X_test, y_train, y_test = train_test_split(X,y, test_size=1-split, stratify=y)


### Do kNN for some range of k.

### From https://campus.datacamp.com/courses/supervised-learning-with-scikit-learn/classification?ex=12
def kNN_range(X_train, y_train, X_test, y_test, ki=1, kf=19):
    # Setup arrays to store train and test accuracies
    neighbors = np.arange(ki, kf)
    train_accuracy = np.empty(len(neighbors))
    test_accuracy = np.empty(len(neighbors))

    # Loop over different values of k
    for i, k in enumerate(neighbors):
        # Setup a k-NN Classifier with k neighbors: knn
        knn = KNeighborsClassifier(n_neighbors=k)#,weights=w)
        # Fit the classifier to the training data
        knn.fit(X_train, y_train)

        #Compute accuracy on the training set
        train_accuracy[i] = knn.score(X_train, y_train)

        #Compute accuracy on the testing set
        test_accuracy[i] = knn.score(X_test, y_test)

    return neighbors, train_accuracy, test_accuracy

ki = 1
kf = 19
neighbors, train_accuracy, test_accuracy = kNN_range(X_train, y_train, X_test, y_test, ki=ki, kf=kf)

# Generate plot
plt.figure()
plt.title('kNN: '+toydataset.capitalize())
plt.plot(neighbors, train_accuracy, label = 'Training')
plt.plot(neighbors, test_accuracy, label = 'Test')
plt.legend()
plt.xlabel('k')
plt.ylabel('Accuracy')
plt.savefig(toydataset+'/kNN_'+toydataset+'_acc.png')
#plt.show()








### Do kNN range for some iterations to smooth out k vs accuracy. This includes resplitting data.
#Neighbors = range(ki,kf)
#Train_accuracy = []
#Test_accuracy = []
kVsAcc = []
#df3 = pd.DataFrame()
#df3['k'] = range(1,19)
iters = 10#500
for i in range(iters):
    X_tr, X_te, y_tr, y_te = train_test_split(X,y, test_size=1-split, stratify=y)
    n, tr_a, te_a = kNN_range(X_tr, y_tr, X_te, y_te, ki=ki, kf=kf)
    #Train_accuracy.append(tr_a)
    #Test_accuracy.append(te_a) 
    kVsAcc.append( (tr_a, te_a) )
    #df3['k'] = 

for i in kVsAcc: i[1][0]

kVsAcc_mean = np.mean(kVsAcc, axis=0)
Train_accuracy = kVsAcc_mean[0]
Test_accuracy = kVsAcc_mean[1]

# Generate plot
plt.figure()
plt.title('Mean kNN (iterations='+str(iters)+'): '+toydataset.capitalize())
plt.plot(neighbors, Train_accuracy, label = 'Training')
plt.plot(neighbors, Test_accuracy, label = 'Test')
plt.legend()
plt.xlabel('k')
plt.ylabel('Accuracy')
plt.savefig(toydataset+'/kNN_'+toydataset+'_acc2.png')#'+str(iters)+'.png')
#plt.show()














### Do kNN with above-found k

# With some k nearest neighbors, create classifier
k = Test_accuracy.argmax()+1 #test_accuracy.argmax()+1 #6
print('')
print('Optimal k =', k)
w = 'distance'
kNN = KNeighborsClassifier(n_neighbors=k)#, weights=w)
# "Fit" the classifier to the data
kNN.fit(X_train, y_train)

# Pass test data (or any data that meets type/form) and have the now-fitted classifier predict targets
y_prediction = kNN.predict(X_test)
y_prediction_probs = kNN.predict_proba(X_test)

# Print accuracy score/report. Score is average of precision of all features(?)
score = kNN.score(X_test, y_test)
#print(score)
report = classification_report(y_test, y_prediction)
print('')
print('Single report:')
print(report)



### Create and plot confusion matrix

# From http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
import itertools
def plot_confusion_matrix(cm,
                          classes=Data.target_names,
                          normalize=False,
                          #title='Confusion matrix',
                          cmap=plt.cm.Paired):#Greys):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        plt.title('Normalized Confusion Matrix: '+toydataset.capitalize())
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        #print("Normalized confusion matrix")
    else:
        plt.title('Confusion Matrix: '+toydataset.capitalize())
        #print('Confusion matrix, without normalization')
    #print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    #plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment='center',
                 color='white' if cm[i, j] > thresh else 'black')
                 #color='black' if cm[i, j] > thresh else 'white')

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')

cmatrix = confusion_matrix(y_test, y_prediction)

plt.figure()
plot_confusion_matrix(cmatrix, normalize=True)#, title='Normalized Confusion Matrix')
plt.savefig(toydataset+'/kNN_'+toydataset+'_cm_k='+str(k)+'.png')
#plt.show()








### Cross validation
folds = 5
kf = KFold(n_splits=folds)#, shuffle=True)
 

y_cvprediction = cross_val_predict(kNN, X, y, cv=folds) #cv=kf)
cvreport = classification_report(Data.target, y_cvprediction)
print('')
print('Cross validation report:')
print(cvreport)

cvscores = cross_val_score(kNN, X, y, cv=folds)
print('Accuracy: %0.2f +- %0.2f' % (cvscores.mean(), cvscores.std()))# * 2))



cv = cross_validate(kNN, X, y, cv=folds)









### Using random forest, find feature importances

### From http://scikit-learn.org/stable/auto_examples/ensemble/plot_forest_importances.html
n_estimators = 250
forest = ExtraTreesClassifier(n_estimators=n_estimators)#int(round(np.sqrt(len(X[0])))))#250)

forest.fit(X, y)
importances = forest.feature_importances_
std = np.std([tree.feature_importances_ for tree in forest.estimators_], axis=0)
indices = np.argsort(importances)[::-1]

# Print the feature ranking
print('')
print('Feature ranking:')
for f in range(X.shape[1]):
    print('%d. Feature %d: %f' % (f + 1, indices[f], importances[indices[f]]))

# Plot the feature importances of the forest
plt.figure()
plt.title('Feature Importance (estimators='+str(n_estimators)+'): '+toydataset.capitalize())
plt.bar(range(X.shape[1]), importances[indices], color="r", yerr=std[indices], align='center')

if 'feature_names' in Data.keys():
    indices2 = [Data.feature_names[i] for i in indices]
    # Remove ' (cm)' from feature names
    if toydataset == 'iris':
        indices2 = [i[:-5] for i in indices2]
    plt.xticks(range(X.shape[1]), indices2)#, rotation=45)
else:
    plt.xticks(range(X.shape[1]), indices)

plt.xlim([-1, X.shape[1]])
plt.savefig(toydataset+'/Forest_'+toydataset+'_featimp.png')
plt.show()

#[Data.feature_names[i] for i in indices]













### Do dummy classification as random baseline

Dummy = DummyClassifier()
# "Fit" the classifier to the data
Dummy.fit(X_train, y_train)

# Pass test data (or any data that meets type/form) and have the now-fitted classifier predict targets
y_prediction_dummy = Dummy.predict(X_test)

# Print accuracy score/report. Score is average of precision of all features(?)
score_dummy = Dummy.score(X_test, y_test)
#print score
report_dummy = classification_report(y_test, y_prediction_dummy)
print('')
print('Single Dummy report:')
print(report_dummy)

