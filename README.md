### Placeholder right now

I will update eventually with an overview of me exploring sklearn and four native datasets.
For now I will paste some poor kNN outputs.

# Iris
>>>
Optimal k = 9

Single report:
             precision    recall  f1-score   support

          0       1.00      1.00      1.00        17
          1       0.94      0.94      0.94        17
          2       0.94      0.94      0.94        17

avg / total       0.96      0.96      0.96        51


Cross validation report:
             precision    recall  f1-score   support

          0       1.00      1.00      1.00        50
          1       0.92      0.96      0.94        50
          2       0.96      0.92      0.94        50

avg / total       0.96      0.96      0.96       150

Accuracy: 0.96 +- 0.03

Feature ranking:
1. Feature 3: 0.464212
2. Feature 2: 0.380387
3. Feature 0: 0.100645
4. Feature 1: 0.054755

Single Dummy report:
             precision    recall  f1-score   support

          0       0.44      0.47      0.46        17
          1       0.45      0.53      0.49        17
          2       0.38      0.29      0.33        17

avg / total       0.43      0.43      0.43        51
>>>


# Digits
>>>
...
>>>


# Wine
>>>
ptimal k = 7

Single report:
             precision    recall  f1-score   support

          0       0.95      1.00      0.98        20
          1       1.00      0.83      0.91        24
          2       0.84      1.00      0.91        16

avg / total       0.94      0.93      0.93        60


Cross validation report:
             precision    recall  f1-score   support

          0       0.94      1.00      0.97        59
          1       1.00      0.92      0.96        71
          2       0.96      1.00      0.98        48

avg / total       0.97      0.97      0.97       178

Accuracy: 0.97 +- 0.03

Feature ranking:
1. Feature 6: 0.151160
2. Feature 12: 0.146073
3. Feature 9: 0.126936
4. Feature 0: 0.118698
5. Feature 11: 0.106696
6. Feature 10: 0.075339
7. Feature 5: 0.059576
8. Feature 1: 0.049053
9. Feature 8: 0.037663
10. Feature 3: 0.037300
11. Feature 4: 0.035530
12. Feature 7: 0.028517
13. Feature 2: 0.027458

Single Dummy report:
             precision    recall  f1-score   support

          0       0.50      0.50      0.50        20
          1       0.46      0.46      0.46        24
          2       0.25      0.25      0.25        16

avg / total       0.42      0.42      0.42        60
>>>


# Breast Cancer
>>>
Optimal k = 4

Single report:
             precision    recall  f1-score   support

          0       0.96      0.96      0.96        71
          1       0.97      0.97      0.97       119

avg / total       0.97      0.97      0.97       190


Cross validation report:
             precision    recall  f1-score   support

          0       0.94      0.95      0.95       212
          1       0.97      0.97      0.97       357

avg / total       0.96      0.96      0.96       569

Accuracy: 0.96 +- 0.02

Feature ranking:
1. Feature 27: 0.106062
2. Feature 22: 0.081792
3. Feature 7: 0.079636
4. Feature 23: 0.073432
5. Feature 20: 0.070256
6. Feature 3: 0.069437
7. Feature 6: 0.060790
8. Feature 0: 0.056035
9. Feature 26: 0.051339
10. Feature 2: 0.048688
11. Feature 13: 0.032044
12. Feature 10: 0.029594
13. Feature 25: 0.028676
14. Feature 21: 0.025785
15. Feature 24: 0.023847
16. Feature 5: 0.023678
17. Feature 1: 0.020132
18. Feature 28: 0.017967
19. Feature 12: 0.015121
20. Feature 29: 0.011489
21. Feature 17: 0.010861
22. Feature 16: 0.010229
23. Feature 4: 0.008224
24. Feature 15: 0.007290
25. Feature 8: 0.007071
26. Feature 19: 0.006457
27. Feature 9: 0.006336
28. Feature 14: 0.006127
29. Feature 18: 0.006108
30. Feature 11: 0.005502

Single Dummy report:
             precision    recall  f1-score   support

          0       0.34      0.35      0.35        71
          1       0.61      0.60      0.60       119

avg / total       0.51      0.51      0.51       190
>>>
